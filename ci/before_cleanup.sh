#!/bin/sh
set -eu

apk add --no-cache curl

REG_VERSION="0.16.1"
REG_SHA256="ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228"
REG_DOWNLOAD_URL="https://github.com/genuinetools/reg/releases/download/v${REG_VERSION}/reg-linux-amd64"
curl --fail --show-error --location ${REG_DOWNLOAD_URL} --output /usr/local/bin/reg
echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c -
chmod a+x /usr/local/bin/reg
