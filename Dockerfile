FROM python:3.9-alpine3.12 as builder

COPY ./requirements.txt /tmp/requirements.txt
RUN apk add gcc libffi-dev libressl-dev musl-dev &&\
    pip3 install --no-warn-script-location --user -r /tmp/requirements.txt &&\
    adduser -S updater -s /bin/nologin -u 1000 &&\
    chown -R 1000 /root/.local

FROM python:3.9-alpine3.12 as runner

RUN adduser -S updater -s /bin/nologin -u 1000
USER 1000

COPY ./updater.py /srv/
COPY --from=builder /root/.local /home/updater/.local

WORKDIR /srv
CMD python3 ./updater.py
