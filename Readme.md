# dynv6-updater
This is an updater script for dynv6 which additionally creates missing `A` and `AAAA` records.

## Features
- set all dns entries to the same IP
- public IPv4 and IPv6 discovery
- multiple dns zones

# Getting started
## Create an HTTP Token for your DNS zone
Login into your dynv6 account and [create an HTTP token.](https://dynv6.com/keys/)

## Configure the mandatory files
Create `dns_entries.txt` and `secrets.yaml`.
```
cp dns_entries.txt.example dns_entries.txt
cp secrets.yaml.example secrets.yaml
```
Insert your desired domain names into `dns_entries.txt`, one name per line.

Insert the dns zone(s) of your desired domain names into `secrets.yaml`.

# Usage
## Run with Python3
```sh
# optionally use a virtualenv
virtualenv -p python3 ~/.venv/dynv6-updater
source ~/.venv/dynv6-updater/bin/activate

# install dependencies
pip3 install -r requirements.txt
# run
python3 updater.py
```

## Run with Docker Compose
Check the `environment` section in `docker-compose.yml` and adapt it to your needs.

### Run Docker Compose in the foreground
```
docker-compose up --build
```

### Run Docker Compose in the background
```
docker-compose up --build -d
```
