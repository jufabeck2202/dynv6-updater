#!/bin/sh
set -eu

# prepare dns_entries.txt
cat <<"EOF" | tee ./dns_entries.txt
test1.gitlab.v6.rocks
test2.gitlab.v6.rocks
test3.special.gitlab.v6.rocks
test4.other.gitlab.v6.rocks
test5.gitlab2.v6.rocks
test6.gitlab2.v6.rocks
test7.special.gitlab2.v6.rocks
test8.other.gitlab2.v6.rocks
EOF

# prepare secrets.yaml
cat <<EOF | tee ./secrets.yaml
dns_zones:
  - name: gitlab.v6.rocks
    token: ${TOKEN_GITLAB_V6_ROCKS}
  - name: gitlab2.v6.rocks
    token: ${TOKEN_GITLAB2_V6_ROCKS}
EOF
